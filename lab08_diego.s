@ Game Genius usando simulador ARM

@ enderecos das portas
	@cores do jogo
	.set LEDS, 0x90110

	@dados
	.set BT_START, 0x90010		@botao start
	.set BT_RED, 0x90020		@botoes das cores do jogo
	.set BT_GREEN, 0x90021
	.set BT_YELLOW, 0x90022
	.set BT_BLUE, 0x90023
	.set TIMER, 0x90008		@ timer 
	
@endereco das interrupcoes
	.set INT_BUTTON, 0x07
	.set INT_TIMER, 0x06	
		
@ constantes
	.set BUFFER_SIZE, 10
	.set MAX_TIME, 900 		@tempo de exibição = MAX_TIME-TIME_DEC*nivel
	.set TIME_DEC, 30
	.set TIME_LIMIT, 2000
@ vetor de interrupcoes
	.org 0x06*4
	b int_timer
	b int_button

	.org 0x1000
@ mascara dos dispositivos
	.set MASK_RED, 0x01		@ máscaras para os botões
	.set MASK_GREEN, 0x02
	.set MASK_YELLOW, 0x04
	.set MASK_BLUE, 0x08
	.set MASK_COLOR_PRESSED, 0x10 	@ máscara que indica que algum botão de cor foi pressionado

	.set MASK_START, 0x20		@ máscara do botão start
	.set MASK_BUTTONS, 0x1F		@ máscara que cobre todos os botões (usada para limpar)
	
	.set MASK_TIMER, 0x40		@ máscara para o timer
	
@ registradores globais
@ r6: estado dos dispositivos
@ r9: nivel atual do jogo (0 a 29)
@ r10: cor atual (0-red, 1-green, 2-yellow, 3-blue)
	
main:
	mov r0, #0		@inicia com record zero
	ldr r1, =record
	str r0, [r1]

resetLabel:	
	@ cria a pilha
	mov sp,#0x10000
	
	@ reseta registradores  globais
	mov r6,#0 	@registrador de estados
	mov r10,#0	@registrador de cor atual
	
	
loop_wait_start:
	mov r0,#MASK_START
	tst r6,r0
	beq loop_wait_start
	
	@ nivel inicial: zero
	mov r9,#0 	@registrador de nivel atual
update_main:
	@carrega cor atual
	@exibe padrao de cores baseado no nivel atual do jogo, make_level()
	bl show_pattern	
	mov r8,#0	@r8 será utilizado para marcar a cor que o usuário deve apertar no momento
loop_main:
	@reseta estado do jogo
	mov r6,#0
	
	@ testa se já verificou todo o input
	cmp r8,r9
	@caso contrário, incrementa o nível e atualiza
	addgt r9,#1
	bgt update_main
	
	@seta timer de fim de jogo TIME_LIMIT
	ldr r1,=TIMER
	ldr r0,=TIME_LIMIT
	str r0,[r1]

	
loop_wait_input:
	@verifica se o tempo acabou
	mov r5,#MASK_TIMER
	tst r6,r5
	@em caso afirmativo, termina o jogo
	bne gameOver
	@verifica se alguma tecla de cor foi pressionada
	mov r5,#MASK_COLOR_PRESSED
	tst r6,r5
	@se nao, tenta ler novamente
	beq loop_wait_input

	@ le proxima cor
	ldr r10,=pattern
	ldr r10,[r10,r8]

	@ avanca estado
	add r8,#1

	@verifica se o bit correspondente a ultima cor está setado
	mov r5,#1
	lsl r5,r10
	tst r6,r5
	
	bne  loop_main

	@fim de jogo
gameOver:
	@imprime a mensagem de fim de jogo
	ldr r1,=msn_gameOver
	mov r2,#len_GOver
	bl print_word

	@ le a maior pontuacao
	ldr r7,=record
	ldr r7,[r7]
	@ compara com a pontuacao atual
	cmp r9,r7
	@se for menor, não faz nada
	ble resetLabel

	@imprime a mensagem de novo recorde
	ldr r1,=msn_record
	mov r2,#len_Record
	bl print_word

	@imprime a nova pontuacao
	mov r3,r9
	bl print_number

	@atualiza o recorde
	ldr r7,=record
	str r9,[r7]
	b resetLabel
	
@ Procedimentos 
@------------------------------------------------------

@ Gera padrao de cores do nivel atual	
make_level:

@ Imprime uma mensagem dado o seu endereço inicial
@ Entrada: r1 - endereço inicial da mensagem
@	   r2 - tamanho da mensagem			
@ Destroi: r0, r7
print_word:
	@ syscall write
	mov r0, #1     @ fd -> stdout
	mov r7, #4     @ write is syscall #4
	svc #0x5555    @ invoke syscall 
	bx lr
	
@ Imprime um número
@ Entrada: r3 - número que será impresso
@ Destroi: r0, r1, r2, r3, r4, r7
print_number:
	@move para o final do buffer
	ldr r1,=buffer
	add r1,#BUFFER_SIZE
	@contador de caracteres
	mov r2,#0

	@quebra de linha
	mov r4,#'\n'	
	strb r4,[r1,-r2]
	add r2,#1
loop_pn:
@r3 = r3/10 e r4 = r3 mod 10
	mov r4,#0
loop_div_pn:
	cmp r3,#10
	subge r3,#10
	addge r4,#1
	bge loop_div_pn
	
	@adiciona um caracter
	add r3,#'0'
	strb r3,[r1,-r2]
	mov r3, r4
	
	add r2,#1 		@avança contador
	cmp r3,#0
	bgt loop_pn

	sub r1,r2		@ atualiza inicio do buffer
	add r1, #1

	@imprime
	mov r4, lr		@ salva endereço de retorno em r4
	bl print_word
	mov lr, r4
	bx lr
	
@ Exibe o padrão até o nível atual (armazenado no registrador r9)
@ Destroi: r0,r1,r3,r4,r5
@ Saida: atualiza r10 para conter a cor do nível atual
show_pattern:
	@seta o timer para MAX_TIME-nivel*TIME_DEC
	ldr r0,=TIMER
	mov r1,#MAX_TIME
	mov r3,#TIME_DEC
	mul r3,r9
	sub r1,r3
	str r1,[r0]
	
	@pega o endereço dos LEDS
	ldr r5,=LEDS
	@seta o nivel da cor atual como zero
	mov r4,#0
loop_sp:
	@le proxima cor
	ldr r0,=pattern
	ldrb r10,[r0,+r4]
	@avança ponteiro de cores
	add r4,#1
	
loop_won_sp:
	@ espera timer
	tst r6,#MASK_TIMER
	beq loop_won_sp
	
	bic r6,#MASK_TIMER
	@acende apenas o led correspondente a cor em r10
	mov r1,#1
	lsl r1,r10
	str r1,[r5]
	
loop_woff_sp:
	@ espera timer
	tst r6,#MASK_TIMER
	beq loop_woff_sp

	bic r6,#MASK_TIMER
	@ apaga leds
	mov r1,#0
	str r1,[r5]
	@ verifica se já exibiu todas as cores para o nível
	cmp r4,r9
	ble loop_sp

	@desabilita o timer
	ldr r0,=TIMER
	mov r1,#0
	str r1,[r0]
	
	bx lr
@ Interrupcoes	
@------------------------------------------------------

@Trata interrupção dos botões
	.align 2
int_button:
	mov sp,#0x11000
	@empilha os registradores
	stmda sp!,{r0,r1,r2}

	ldr r0, =BT_START	@ verifica se o botão start foi pressionado
	ldrb r0, [r0]		@ se sim, seta o bit correspondente em r6
	cmp r0, #1		@ e retorna da interrupção
	orreq r6, #MASK_START
	moveqs pc, lr
	
	ldr r1, =BT_RED		@ primeira cor	
	ldr r0, =MASK_RED	@ máscara correspondente a primeira cor
loop_IB:
	ldrb r2, [r1]		@ verifica qual dos botões correspondentes às cores
	cmp r2, #1		@ foi pressionado. Seta bit em r6 para 1 se foi 
	orreq r6, r0		@ pressionado, ou seta para 0 caso contrário
	bicne r6, r0
	
	ldr r2, =BT_BLUE	@ última cor
	cmp r1, r2
	addne r1, #1
	lslne r0, #1
	bne loop_IB

	orr r6, #MASK_COLOR_PRESSED	@ botão correspondente a alguma das cores foi  pressionado
	ldmib sp!,{r2}
	ldmib sp!,{r1}
	ldmib sp!,{r0}
	movs pc, lr

	
@Trata interrupção do timer
	.align 2
int_timer:
	orr r6, #MASK_TIMER		@ seta bit do timer em r6
	movs pc, lr
	
@------------------------------------------------------

	.align 2
pattern:
	.byte 0,1,0,2,3,0,1,2,3,0,1,2,0,1,2,0,0,0,2,3,1,2,2,3,0,1,2,0,3,2,1,2,3,2,2,0,1

	.align 2
msn_gameOver:
	.ascii 	"Game over\n"
len_GOver = . - msn_gameOver

	.align 2
msn_record:
	.ascii 	"New Record\n"	
len_Record = . - msn_record

	.align 2
buffer:
	.skip 20
record:
	.word 0
	
